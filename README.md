# TABULA ![Build Status](https://gitlab.com/tabula/overlay/badges/master/build.svg)

![TS4GUFM-V](https://gitlab.com/tabula/overlay/raw/master/docs/ts4gufm-v.jpg)

**tabula** is a [Gentoo Linux](https://gentoo.org) overlay for Tabula disk image.

**THIS IS AN INTERNAL OVERLAY!!!**

* * *

## Contents

### sys-kernel/genkernel-next

Ebuild for my [fork](https://gitlab.com/tabula/genkernel-next) of [genkernel-next](https://github.com/Sabayon/genkernel-next).

This fork adds boot from AUFS root support.

Difference from original genkernel-next:

- [x] boot from AUFS by setting `aufs` option, `root` is a root filesystem, `over_root` is an overlay;
- [x] added `over_root`, `over_rootfstype` and `over_rootflags` options;
- [x] added `MARKER=marker` filesystem discovery, thats searches block device by file (example: `root=MARKER=markerkkNAPIne` resolves block device which contains on root file `markerkkNAPIne`).

Example GRUB2 configuration:

```bash
menuentry 'Gentoo GNU/Linux, with Linux x86_64-4.4.6-gentoo' --class gentoo --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-x86_64-4.4.6-gentoo-advanced-/dev/sdb4' {
	load_video
	if [ "x$grub_platform" = xefi ]; then
		set gfxpayload=keep
	fi
	insmod gzio
	insmod part_gpt
	insmod ext2
	set root='hd1,gpt3'
	if [ x$feature_platform_search_hint = xy ]; then
	  search --no-floppy --fs-uuid --set=root  bdffe021-3836-4b87-b8bf-156b3db9675a
	else
	  search --no-floppy --fs-uuid --set=root bdffe021-3836-4b87-b8bf-156b3db9675a
	fi
	echo	'Loading Linux x86_64-4.4.6-gentoo ...'
	linux	/kernel-genkernel-x86_64-4.4.6-gentoo aufs root=MARKER=.ro over_root=UUID=41e0b5cf-8e85-4bb9-ac53-e5a92df26f3b domdadm dolvm
	echo	'Loading initial ramdisk ...'
	initrd	/initramfs-genkernel-x86_64-4.4.6-gentoo
}
```

### sys-process/netatop

Kernel module to collect per process network statistics for atop.

* * *

## Installation

### app-portage/layman

To add our overlay via [layman](https://wiki.gentoo.org/wiki/Layman) just do:

```
layman -o https://gitlab.com/tabula/overlay/raw/master/docs/layman.xml -f -a tabula
```

### sys-apps/portage

Since version 2.2.16, Portage has a native mechanism for adding overlays.

Just copy `docs/repos.conf` to `/etc/portage/repos.conf/tabula.conf`:

```
mkdir -p /etc/portage/repos.conf
wget https://gitlab.com/tabula/overlay/raw/master/docs/repos.conf -O /etc/portage/repos.conf/tabula.conf
```

Edit the "path" variable in `/etc/portage/repos.conf/tabula.conf` to suit your overlay storage path
(for example, `/usr/local/portage/tabula`).
Now you can sync the overlay using `emaint sync -r tabula`.
